package dev.rhacs.springboot.validation.example.models.errors;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY, content = JsonInclude.Include.NON_EMPTY)
public class InvalidParameter {

    // Attributes
    // ----------------------------------------------------------------------------------

    /**
     * The name of the field which caused the error
     */
    private String fieldName;

    /**
     * The detailed information of the error
     */
    private String faultMessage;

    /**
     * The rejected value
     */
    private Object rejectedValue;

    // Constructors
    // ----------------------------------------------------------------------------------

    /**
     * Creates a new {@link InvalidParameter} instance
     *
     * @param faultMessage The detailed information of the error
     */
    public InvalidParameter(String faultMessage) {
        this.faultMessage = faultMessage;
    }

    /**
     * Creates a new {@link InvalidParameter} instance
     *
     * @param faultMessage The detailed information of the error
     * @param fieldName    The name of the field which caused the error
     */
    public InvalidParameter(String faultMessage, String fieldName) {
        this(faultMessage);
        this.fieldName = fieldName;
    }

    /**
     * Creates a new {@link InvalidParameter} instance
     *
     * @param faultMessage  The detailed information of the error
     * @param fieldName     The name of the field which caused the error
     * @param rejectedValue The rejected value
     */
    public InvalidParameter(String faultMessage, String fieldName, Object rejectedValue) {
        this(faultMessage, fieldName);
        this.rejectedValue = rejectedValue;
    }

}

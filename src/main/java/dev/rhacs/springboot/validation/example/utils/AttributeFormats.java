package dev.rhacs.springboot.validation.example.utils;

import lombok.experimental.UtilityClass;

/**
 * Contains the formats and regular expressions that are going to be used to validate attributes and format response messages
 */
@UtilityClass
public class AttributeFormats {

    /**
     * Email Address Regular Expression.
     *
     * @see <a href="https://owasp.org/www-community/OWASP_Validation_Regex_Repository">OWASP Validation Regex Repository</a>
     */
    public static final String FORMAT_EMAIL_ADDRESS = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

    /**
     * Timestamp format that complies with the RFC3339 document from the IETF
     *
     * @see <a href="https://datatracker.ietf.org/doc/html/rfc3339#section-5.6">Date and Time on the Internet: Timestamps</a>
     */
    public static final String FORMAT_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

}

package dev.rhacs.springboot.validation.example.models.dtos;

import dev.rhacs.springboot.validation.example.models.enums.Gender;
import dev.rhacs.springboot.validation.example.utils.AttributeFormats;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.Set;

/**
 * Represents a Contact Information in the system
 */
@Data
public class ContactInformation {

    /**
     * The first name
     */
    @NotBlank
    @Size(min = 2, max = 100)
    private String firstName;

    /**
     * The last name
     */
    @NotBlank
    @Size(min = 2, max = 100)
    private String lastName;

    /**
     * The email address
     */
    @NotBlank
    @Email(regexp = AttributeFormats.FORMAT_EMAIL_ADDRESS)
    private String emailAddress;

    /**
     * The {@link Gender}
     */
    @NotNull
    private Gender gender;

    /**
     * A list of {@link PhoneNumber}s
     */
    @Valid
    @NotEmpty
    @Size(min = 1, max = 10)
    private Set<PhoneNumber> phoneNumbers;

}

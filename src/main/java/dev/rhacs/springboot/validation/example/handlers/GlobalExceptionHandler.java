package dev.rhacs.springboot.validation.example.handlers;

import dev.rhacs.springboot.validation.example.models.errors.ErrorResponse;
import dev.rhacs.springboot.validation.example.utils.GenericMessages;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    // Methods
    // ----------------------------------------------------------------------------------

    /**
     * Extracts the request method from the {@link WebRequest} object
     *
     * @param request The {@link WebRequest} object that contains the information of the request made by the client
     * @return a {@link String} representation of the request method
     */
    private String extractRequestMethod(WebRequest request) {
        return ((ServletWebRequest) request).getRequest().getMethod();
    }

    // Overrides
    // ----------------------------------------------------------------------------------

    /**
     * Handles {@link HttpMessageNotReadableException}
     *
     * @param ex      An {@link HttpMessageNotReadableException} object that contains the information of the error
     * @param headers The {@link HttpHeaders} sent by the client
     * @param status  The {@link HttpStatus} of the Response
     * @param request The {@link WebRequest} made by the client
     * @return a {@link ResponseEntity} that contains the details of the error for the client
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String requestMethod = extractRequestMethod(request);

        ErrorResponse errorResponse = new ErrorResponse(status, GenericMessages.EMPTY_OR_WRONG, requestMethod);
        errorResponse.addInvalidParameter(ex.getMostSpecificCause().getMessage());

        return ResponseEntity.badRequest().body(errorResponse);
    }

    /**
     * Handles {@link MethodArgumentNotValidException}
     *
     * @param ex      The {@link MethodArgumentNotValidException} object that contains the information of the exception
     * @param headers The {@link HttpHeaders} sent by the client
     * @param status  The {@link HttpStatus} of the Response
     * @param request The {@link WebRequest} made by the client
     * @return A {@link ResponseEntity} that contains the information of the error for the client
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String requestMethod = extractRequestMethod(request);

        ErrorResponse errorResponse = new ErrorResponse(status, GenericMessages.EMPTY_OR_WRONG, requestMethod);

        ex.getGlobalErrors().forEach(errorResponse::addInvalidParameter);
        ex.getFieldErrors().forEach(errorResponse::addInvalidParameter);

        return ResponseEntity.badRequest().body(errorResponse);
    }

    /**
     * Handles {@link HttpRequestMethodNotSupportedException}
     *
     * @param ex      The {@link HttpRequestMethodNotSupportedException} object that contains the information of the exception
     * @param headers The {@link HttpHeaders} sent by the client
     * @param status  The {@link HttpStatus} of the Response
     * @param request The {@link WebRequest} sent by the client
     * @return A {@link ResponseEntity} object that contains the information of the error for the client
     */
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(status, ex.getMessage(), ex.getMethod());
        return ResponseEntity.status(status).body(errorResponse);
    }

    /**
     * Handles {@link HttpMediaTypeNotSupportedException}
     *
     * @param ex      The {@link HttpMediaTypeNotSupportedException} object that contains the information of the exception
     * @param headers The {@link HttpHeaders} sent by the client
     * @param status  The {@link HttpStatus} of the Response
     * @param request The {@link WebRequest} made by the client
     * @return A {@link ResponseEntity} object that contains the information of the error for the client
     */
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(status, ex.getMessage(), extractRequestMethod(request));
        return ResponseEntity.status(status).body(errorResponse);
    }

    /**
     * Handles {@link NoHandlerFoundException}
     *
     * @param ex      The {@link NoHandlerFoundException} object that contains the information of the exception
     * @param headers The {@link HttpHeaders} sent by the client
     * @param status  The {@link HttpStatus} of the Response
     * @param request The {@link WebRequest} sent by the client
     * @return A {@link ResponseEntity} object that contains the information of the error that's going to be sent to the client
     */
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(status, ex.getMessage(), ex.getHttpMethod());
        return ResponseEntity.status(status).body(errorResponse);
    }

}

package dev.rhacs.springboot.validation.example.models.dtos;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Represents a Phone Number in the system
 */
@Data
public class PhoneNumber {

    /**
     * The country code to which the phone number belongs
     */
    @NotNull
    @Min(1)
    @Max(998)
    private Integer countryCode;

    /**
     * The area/region code of the {@link PhoneNumber}
     */
    @NotNull
    @Min(1)
    @Max(2000)
    private Integer areaCode;

    /**
     * The actual {@link PhoneNumber}
     */
    @NotNull
    @Min(1000000)
    @Max(9999999)
    private Long number;

    /**
     * Indicates whether the {@link PhoneNumber} is for personal or public use
     */
    @NotNull
    private Boolean personal;

}

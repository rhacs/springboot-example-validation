package dev.rhacs.springboot.validation.example.controllers;

import dev.rhacs.springboot.validation.example.models.dtos.ContactInformation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = {"/contact"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ContactInformationController {

    @PostMapping
    public ResponseEntity<ContactInformation> create(@Valid @RequestBody ContactInformation contactInformation) {
        return ResponseEntity.status(HttpStatus.CREATED).body(contactInformation);
    }

}

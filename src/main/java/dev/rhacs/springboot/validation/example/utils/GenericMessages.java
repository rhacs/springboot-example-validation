package dev.rhacs.springboot.validation.example.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class GenericMessages {

    /**
     * Message sent when the request contains empty or wrong values for the required parameters
     */
    public static final String EMPTY_OR_WRONG = "One or more parameters were empty or wrong, please check";

}

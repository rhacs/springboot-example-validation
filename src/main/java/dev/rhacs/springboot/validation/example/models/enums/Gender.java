package dev.rhacs.springboot.validation.example.models.enums;

public enum Gender {
    FEMALE,
    MALE
}

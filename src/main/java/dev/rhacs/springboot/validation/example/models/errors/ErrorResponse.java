package dev.rhacs.springboot.validation.example.models.errors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import dev.rhacs.springboot.validation.example.utils.AttributeFormats;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY, content = JsonInclude.Include.NON_EMPTY)
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
@JsonTypeName(value = "error")
public class ErrorResponse {

    // Constructors
    // ----------------------------------------------------------------------------------

    /**
     * Instant when the error occurred
     */
    @JsonFormat(pattern = AttributeFormats.FORMAT_TIMESTAMP, shape = JsonFormat.Shape.STRING)
    private ZonedDateTime timestamp;

    /**
     * HTTP Status Code
     */
    private int code;

    /**
     * String representation of the HTTP Status Code
     */
    private String status;

    /**
     * The request method used by the client
     */
    private String requestMethod;

    /**
     * Generic/Categorization message of the error
     */
    private String message;

    /**
     * A list of {@link InvalidParameter}s that contain detailed information
     */
    private Set<InvalidParameter> invalidParameters;

    // Constructors
    // ----------------------------------------------------------------------------------

    /**
     * Creates a new {@link ErrorResponse} instance
     *
     * @param httpStatus    An {@link HttpStatus} enum that contains the error code and the name of the HTTP Status Code
     * @param message       The generic/categorization message of the error
     * @param requestMethod The request method used by the client
     */
    public ErrorResponse(HttpStatus httpStatus, String message, String requestMethod) {
        this.code = httpStatus.value();
        this.status = httpStatus.getReasonPhrase();
        this.message = message;
        this.requestMethod = requestMethod;

        this.timestamp = ZonedDateTime.now();
        this.invalidParameters = new HashSet<>();
    }

    // Methods
    // ----------------------------------------------------------------------------------

    /**
     * Adds a new {@link InvalidParameter} to the list
     *
     * @param message The detailed information of the error
     */
    public void addInvalidParameter(String message) {
        this.invalidParameters.add(new InvalidParameter(message));
    }

    /**
     * Adds a new {@link InvalidParameter} to the list
     *
     * @param message   The detailed information of the error
     * @param fieldName The name of the field which caused the error
     */
    public void addInvalidParameter(String message, String fieldName) {
        this.invalidParameters.add(new InvalidParameter(message, fieldName));
    }

    /**
     * Adds a new {@link InvalidParameter} to the list
     *
     * @param message       The detailed information of the error
     * @param fieldName     The name of the field which caused the error
     * @param rejectedValue The rejected value
     */
    public void addInvalidParameter(String message, String fieldName, Object rejectedValue) {
        this.invalidParameters.add(new InvalidParameter(message, fieldName, rejectedValue));
    }

    /**
     * Adds a {@link FieldError} to the {@link InvalidParameter} list
     *
     * @param fieldError A {@link FieldError} object that contains the information of the error
     */
    public void addInvalidParameter(FieldError fieldError) {
        this.addInvalidParameter(fieldError.getDefaultMessage(), fieldError.getField(), fieldError.getRejectedValue());
    }

    /**
     * Adds an {@link ObjectError} to the {@link InvalidParameter} list
     *
     * @param objectError An {@link ObjectError} that contains the information of the error
     */
    public void addInvalidParameter(ObjectError objectError) {
        this.addInvalidParameter(objectError.getDefaultMessage(), objectError.getObjectName());
    }

}
